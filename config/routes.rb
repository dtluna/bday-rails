Rails.application.routes.draw do
  resources :birthday_cards
  root 'birthday_cards#index'
end
