class CreateBirthdayCards < ActiveRecord::Migration
  def change
    create_table :birthday_cards do |t|
      t.attachment :image
      t.string :title
      t.text :greeting
      t.string :author

      t.timestamps null: false
    end
  end
end
