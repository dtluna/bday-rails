require 'test_helper'

class BirthdayCardsControllerTest < ActionController::TestCase
  setup do
    @birthday_card = birthday_cards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:birthday_cards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create birthday_card" do
    assert_difference('BirthdayCard.count') do
      post :create, birthday_card: { author: @birthday_card.author, greeting: @birthday_card.greeting, image: @birthday_card.image, title: @birthday_card.title }
    end

    assert_redirected_to birthday_card_path(assigns(:birthday_card))
  end

  test "should show birthday_card" do
    get :show, id: @birthday_card
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @birthday_card
    assert_response :success
  end

  test "should update birthday_card" do
    patch :update, id: @birthday_card, birthday_card: { author: @birthday_card.author, greeting: @birthday_card.greeting, image: @birthday_card.image, title: @birthday_card.title }
    assert_redirected_to birthday_card_path(assigns(:birthday_card))
  end

  test "should destroy birthday_card" do
    assert_difference('BirthdayCard.count', -1) do
      delete :destroy, id: @birthday_card
    end

    assert_redirected_to birthday_cards_path
  end
end
