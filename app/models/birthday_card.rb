class BirthdayCard < ActiveRecord::Base
  validates :title, presence: true
  validates :greeting, presence: true
  validates :author, presence: true
  has_attached_file :image,
    styles: { medium: "300x300>", thumb: "100x100>" },
    default_style: :medium
  validates_attachment :image, presence: true,
    content_type: { content_type: /\Aimage/ },
    file_name: { matches: [/png\Z/, /jpe?g\Z/, /gif\Z/] },
    size: { in: 0..5.megabytes }
end
