json.array!(@birthday_cards) do |birthday_card|
  json.extract! birthday_card, :id, :image, :title, :greeting, :author
  json.url birthday_card_url(birthday_card, format: :json)
end
