class BirthdayCardsController < ApplicationController
  before_action :set_birthday_card, only: [:show, :edit, :update, :destroy]

  # GET /birthday_cards
  # GET /birthday_cards.json
  def index 
    @birthday_cards = BirthdayCard.order(:created_at).reverse_order.page(params[:page])
  end

  # GET /birthday_cards/1
  # GET /birthday_cards/1.json
  def show
  end

  # GET /birthday_cards/new
  def new
    @birthday_card = BirthdayCard.new
  end

  # # GET /birthday_cards/1/edit
  def edit
  end

  # POST /birthday_cards
  # POST /birthday_cards.json
  def create
    @birthday_card = BirthdayCard.new(birthday_card_params)

    respond_to do |format|
      if @birthday_card.save
        format.html { redirect_to @birthday_card, notice: 'Birthday card was successfully created.' }
        format.json { render :show, status: :created, location: @birthday_card }
      else
        format.html { render :new }
        format.json { render json: @birthday_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /birthday_cards/1
  # PATCH/PUT /birthday_cards/1.json
  def update
    respond_to do |format|
      if @birthday_card.update(birthday_card_params)
        format.html { redirect_to @birthday_card, notice: 'Birthday card was successfully updated.' }
        format.json { render :show, status: :ok, location: @birthday_card }
      else
        format.html { render :edit }
        format.json { render json: @birthday_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /birthday_cards/1
  # DELETE /birthday_cards/1.json
  def destroy
    @birthday_card.destroy
    respond_to do |format|
      format.html { redirect_to birthday_cards_url, notice: 'Birthday card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_birthday_card
      @birthday_card = BirthdayCard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def birthday_card_params
      params.require(:birthday_card).permit(:image, :title, :greeting, :author)
    end
end
